namespace IsilonApiBuilder.Models
{
    public class System
    {
        public double cpu { get; set; }
        public double disk_in { get; set; }
        public double disk_out { get; set; }
        public double ftp { get; set; }
        public double hdfs { get; set; }
        public double http { get; set; }
        public double net_in { get; set; }
        public double net_out { get; set; }
        public double nfs { get; set; }
        public string node { get; set; }
        public double smb { get; set; }
        public int time { get; set; }
        public double total { get; set; }
    }
}