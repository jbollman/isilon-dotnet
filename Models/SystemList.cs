using System.Collections;
using System.Collections.Generic;

namespace IsilonApiBuilder.Models
{
    public class SystemList : IEnumerable
    {
        public List<System> System { get; set; }
        
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable) this).GetEnumerator();
        }
    }
}