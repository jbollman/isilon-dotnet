using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection.Metadata.Ecma335;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using IsilonApiBuilder.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Authenticators;

namespace IsilonApiBuilder
{
    public class Cluster
    {
        private HttpClient _httpClient;
        private readonly string _userName;
        private readonly string _password;
        public Cluster(Auth auth)
        {
            _userName = auth.username;
            _password = auth.password;
        }

        public HttpClient HttplicentAccount
        {
            get
            {
                _httpClient = _httpClient ?? new HttpClient
                              (
                                  new HttpClientHandler()
                                  {
                                      ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) =>
                                      {
                                          //bypass
                                          return true;
                                      },
                                  }
                                  , false
                              )
                              {
                                  BaseAddress = new Uri("http://10.7.0.216:8080/platform/5/statistics/summary/system"),
                              };

                // In case you need to send an auth token...
                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                    Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes($"{_userName}:{_password}")));
                return _httpClient;
            }
        }
        //public async Task<SystemList>  GeneralClusterStats(Auth auth)
        //public async Task<string>  GeneralClusterStats(Auth auth)
        public async Task GeneralClusterStats()
        {
            const string URL = "https://10.7.0.216:8080/platform/5/statistics/summary/system";
            
            
            try
            {
                var request = new HttpRequestMessage();
                request.RequestUri = new Uri(URL);
                request.Method = HttpMethod.Get;
                request.Headers.Add("Accept", "*/*");


                // Attempting to handle SSL issues

                var httpClientHandler = new HttpClientHandler();
                httpClientHandler.SslProtocols = SslProtocols.Tls12 | SslProtocols.Tls11 | SslProtocols.Tls;
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) =>
                {
                    return true;
                };

                //passing in basic auth 
                var ahv = new AuthenticationHeaderValue("Basic",
                    Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes($"{_userName}:{_password}")));

                var _httpClient = new HttpClient(httpClientHandler);
                
                
                //var _httpClient = new HttpClient();
 
                _httpClient.DefaultRequestHeaders.Authorization = ahv;
                _httpClient.Timeout = TimeSpan.FromMinutes(10);

               
                
                
                
                HttpResponseMessage response = await _httpClient.GetAsync(URL);
                var content = response.Content;
                string json = await content.ReadAsStringAsync();
                //var wrapper = JsonConvert.DeserializeObject<SystemList>(json);
                //var oc = new ObservableCollection<SystemList>(wrapper.System);
                JObject parsed = JObject.Parse(json);
                foreach (var pair in parsed)
                {  
                    Console.WriteLine("{0}: {1}", pair.Key, pair.Value);    
                        
                }  //foreach
                
                

            }
            catch (Exception ex)
            {
                using (System.IO.StreamWriter file =
                    new System.IO.StreamWriter(@"/Users/jbollman/my-dev/logs/isilon-exception.txt"))
                {
                    file.WriteLine(ex.Message);
                    file.WriteLine(ex.StackTrace);
                }
            }
            finally
            {
                _httpClient.Dispose();
            }




        }

        public string RestSharpGeneralClusterStats()
        {
            var client = new RestClient("https://10.7.0.216:8080/platform/5/statistics/summary/system");
            client.Authenticator = new SimpleAuthenticator("username", "admin", "password", "#L#M#Ntek1");

            var request = new RestRequest("resource", Method.GET);
            var response = client.Execute(request);
            Console.WriteLine(response.Content);
            return response.Content;
        }

        public async Task HttpLab()
        {
            const string URL = "https://10.7.0.216:8080/platform/5/statistics/summary/system";
            
            
            var ahv = new AuthenticationHeaderValue("Basic",
                Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes($"{_userName}:{_password}")));
            
            var nClient = new HttpClient();
            nClient.DefaultRequestHeaders.Authorization = ahv;
            //HttpResponseMessage nresponse = await nClient.GetAsync("http://stackoverflow.com", HttpCompletionOption.ResponseHeadersRead);
            HttpResponseMessage nresponse = await nClient.GetAsync("http://stackoverflow.com");
            if (nresponse.StatusCode != HttpStatusCode.Accepted)
                Console.WriteLine(nresponse.StatusCode);

        }
    }
    
    
}