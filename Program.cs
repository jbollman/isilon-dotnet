﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

// ReSharper disable once IdentifierTypo
namespace IsilonApiBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            //Program program = new Program();
            //var content = program.Get(@"http://www.google.com");
            //Console.ReadLine();
            //Console.WriteLine(content);
            //Console.ReadLine();
            //Console.WriteLine("\n" +  content.Result);
            //Console.WriteLine("Program finished");

            var auth = new Auth();
            auth.username = "admin";
            auth.password = "#L#M#Ntek1";
            
            //HttpCompletionOption.ResponseHeadersRead).ConfigureAwait(false);
            
            var cluster = new Cluster(auth);
            cluster.GeneralClusterStats().Wait();

            // cluster.HttpLab().Wait();
        }

        public async Task<HttpContent> Get(string url)
        {
            using (HttpClient client = new HttpClient())
            using (HttpResponseMessage response = await client.GetAsync(url).ConfigureAwait(false))
            using (HttpContent content = response.Content)
            {
                Console.WriteLine(content);
                return content;
            }
        }
    }
}
